import { Request, Response } from 'express'
import { Message } from '../entity/message'
import { getRepository } from '../db/connection'

function getNumber(val: string | undefined, defaultVal: number): number {
  if (val) {
    const num = parseInt(val, 10)
    if (!isNaN(num)) {
      return num
    }
  }
  return defaultVal
}

export default async (req: Request, res: Response): Promise<Response> => {
  const skip = getNumber(req.query.skip, 0)
  const take = getNumber(req.query.take, 20)
  const repository = await getRepository(Message)
  const results = await repository.findAndCount({
    order: { id: 'DESC' },
    skip,
    take,
  })
  return res.status(200).send(results)
}
