import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id: number = 0

  @Column('text')
  message: string = ''
}
