import * as dotenv from 'dotenv'
dotenv.config()

import * as Express from 'express'
import * as helmet from 'helmet'
import * as bodyParser from 'body-parser'
import * as compression from 'compression'

import { NOT_FOUND, getStatusText } from 'http-status-codes'

import api from './api'

const app: Express.Application = Express()

app.use(helmet())
app.use(compression())

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api', api)

app.use(function(
  req: Express.Request,
  res: Express.Response,
  next: Express.NextFunction
) {
  res.status(NOT_FOUND).send(getStatusText(NOT_FOUND))
})

if (process.env.NODE_ENV !== 'test') {
  app.listen(3000, '0.0.0.0')
  console.log('server started: http://0.0.0.0:3000')
}

export default app
